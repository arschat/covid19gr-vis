# covid19gr visualization

**ggplot visualizations of COVID19 data**

More to be added. Almost only greek data.
Data derived from [@sandbird](https://github.com/sandbird) or [Nyrros Covid Team](https://twitter.com/NyrrosC)

---
## Compare with cumulative cases
![Cases vs Date](img/group_cumulative_cases.png?raw=true "Groups of cumulative Cases")
![](img/custom_cumulative_cases.png?raw=true "Compare date ranges with equal cumulative number of cases")
Devide the number of covid cases in a user-defined value of cumulative cases and find dates where this was surpassed

[R script](https://gitlab.com/arschat/covid19gr-vis/-/blob/main/group_cumulative_cases.R) - [shiny app](https://arschat.shinyapps.io/Cumulative_covid19_greek_cases/)
